# The following outputs allow authentication and connectivity to the GKE Cluster
# by using certificate-based authentication.
# See the instructions at:
# https://docs.gitlab.com/ee/user/project/clusters/index.html#adding-an-existing-kubernetes-cluster
output "cluster_name" {
  value = "${google_container_cluster.gitlab_gke_cluster.name}"
}

output "environment_scope" {
  value = "${var.environment}"
}

output "api_url" {
  value = "${format("https://%s", google_container_cluster.gitlab_gke_cluster.endpoint)}"
}

output "cluster_ca_certificate" {
  value = "${base64decode(google_container_cluster.gitlab_gke_cluster.master_auth.0.cluster_ca_certificate)}"
}

output "project_namespace" {
  value = "${var.project_namespace}"
}

output "service_token" {
  value = "${data.kubernetes_secret.gitlab_admin.data.token}"
}
output "json.key" {
    value = <<EOF
provider: Google
google_project: ${var.gcp_project_id}
google_client_email: gitlab-gcs@${var.gcp_project_id}.iam.gserviceaccount.com
google_json_key_string: '${base64decode(google_service_account_key.gitlab-gcr.private_key)}'
EOF
}
