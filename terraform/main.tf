provider "google" {
  version = "~> 2.3.0"
  project = "${var.gcp_project_id}"
  region  = "${var.gcp_region}"
}

provider "google-beta" {
  version = "~> 2.3.0"
  project = "${var.gcp_project_id}"
  region  = "${var.gcp_region}"
}

# Provides access to available Google Container Engine versions in a zone for a given project.
# https://www.terraform.io/docs/providers/google/d/google_container_engine_versions.html
# Doesn't work until the default context is setup
data "google_container_engine_versions" "region" {
  location = "${var.gcp_zone}"
}

# Define a 20 character random admin password for the cluster
resource "random_string" "password" {
  length  = 20
  special = false # Some special characters don't work yet

  # https://github.com/google-terraform-modules/terraform-google-kubernetes-engine/issues/18
  number = true
  lower  = true
  upper  = true
}

# Create preemptible node pool for gitlab
resource "google_container_node_pool" "gitlab_premptible_nodes" {
  name               = "${format("%s-premptible-node-pool", var.cluster_name)}"
  version            = "${data.google_container_engine_versions.region.latest_node_version}"
  #version            = "${var.gke_version}"
  location           = "${var.gcp_zone}"
  cluster            = "${google_container_cluster.gitlab_gke_cluster.name}"
  initial_node_count = 2

  autoscaling {
    min_node_count = 1
    max_node_count = 5
  }

  management {
    auto_repair  = true
    auto_upgrade = false # Don't auto upgrade kubernetes versions, or terraform fights you.
  }

  node_config {
    machine_type = "n1-standard-1"
    disk_size_gb = 32

    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/trace.append",
    ]

    preemptible = true # These nodes are 1/5th the cost but they restart every 24 hours

    # Be helpful and label and tag this cluster so we can work with it based on
    # metadata -- these are arbitrarily selected based on what I've seen other
    # operations teams use in the past. Add whatever you think works at DYL.
    labels = "${merge(var.labels,
      map("component", "gitlab_premptible_nodes"),
      map("name", "gitlab-ci-cd"),
  		map("instance", "${var.gcp_zone}"),
  		map("app-role", "premptible-node-pool"),
      map("heritage", "terraform")
  )}"

    tags = ["${var.cluster_name}", "${var.environment}"]
  }
}

# Create a one to five node GKE cluster
# GitLab recommends 6 CPU with 16G RAM, use 5 n1-standard-1 instances
# https://gitlab.com/charts/gitlab/blob/master/doc/installation/index.md

resource "google_container_cluster" "gitlab_gke_cluster" {
  name = "${var.cluster_name}"

  location = "${var.gcp_zone}"

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true

  initial_node_count = 1

  # Setting an empty username and password explicitly disables basic auth
  master_auth {
    username = "admin"
    password = "${random_string.password.result}"

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  network    = "${format("projects/%s/global/networks/default", var.gcp_project_id)}"
  subnetwork = "${format("projects/%s/regions/%s/subnetworks/default", var.gcp_project_id, var.gcp_region)}"

  # This obtains the latest versions of GKE being used in this region.
  min_master_version = "${data.google_container_engine_versions.region.latest_master_version}"
  #min_master_version = "${var.gke_version}"

  # Don't set this, because the default-pool is being destroyed
  #node_version       = "${data.google_container_engine_versions.region.latest_node_version}"

  addons_config {
    horizontal_pod_autoscaling {
      disabled = "false"
    }

    http_load_balancing {
      disabled = "false"
    }

    kubernetes_dashboard {
      disabled = "false"
    }

    network_policy_config {
      disabled = "true"
    }
  }
  maintenance_policy {
    daily_maintenance_window {
      start_time = "05:30"
    }
  }
  enable_kubernetes_alpha = "false"
  enable_legacy_abac      = "true"
  ip_allocation_policy {
    use_ip_aliases = "true"
  }
  logging_service    = "logging.googleapis.com/kubernetes"
  monitoring_service = "monitoring.googleapis.com/kubernetes"
  lifecycle {
    create_before_destroy = true
    prevent_destroy       = false

    ignore_changes = ["node_pool"]
  }
}

provider "kubernetes" {
}

# Create the namespace where apps will be deployed
resource "kubernetes_namespace" "gitlab" {
  metadata {
    annotations {
      env = "${var.environment}"
    }

    labels = "${merge(var.labels,
                        map("application-name", "AutoDevOps"),
                        map("component", "gitlab-kubernetes_namespace"),
                        map("name", "gitlab"),
                        map("instance", "${var.gcp_zone}"),
                        map("part-of", "gitlab"),
                        map("app-role", "namespace")
    )}"

    name = "${var.project_namespace}"
  }
}

# Create a service account for gitlab autodevops
resource "kubernetes_service_account" "gitlab_admin" {
  metadata {
    name      = "gitlab-admin"
    namespace = "${var.project_namespace}"

    labels = "${merge(var.labels,
			map("application-name", "AutoDevOps"),
	  	map("component", "gitlab-kubernetes_service_account"),
	  	map("name", "gitlab-admin"),
	  	map("instance", "${var.gcp_zone}"),
	  	map("part-of", "gitlab"),
	  	map("app-role", "account")
  )}"
  }

  # Fix bug https://github.com/terraform-providers/terraform-provider-helm/issues/148
  automount_service_account_token = true
}

resource "kubernetes_cluster_role_binding" "gitlab_admin" {
  metadata {
    name = "gitlab-admin"

    labels = "${merge(var.labels,
			map("application-name", "GitLab"),
		  map("component", "gitlab-kubernetes_cluster_role_binding"),
		  map("name", "gitlab-admin"),
		  map("instance", "${var.gcp_zone}"),
	  	map("part-of", "gitlab"),
		  map("app-role", "rbac")
    )}"
  }

  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = "${kubernetes_service_account.gitlab_admin.metadata.0.name}"
    namespace = "${kubernetes_service_account.gitlab_admin.metadata.0.namespace}"
  }
}

data "kubernetes_secret" "gitlab_admin" {
  metadata {
    name      = "${kubernetes_service_account.gitlab_admin.default_secret_name}"
    namespace = "${kubernetes_service_account.gitlab_admin.metadata.0.namespace}"
  }
}

# See https://cloud.google.com/solutions/deploying-production-ready-gitlab-on-gke
# Create a service account for accessing Google Cloud Registry
resource "google_service_account" "gitlab-gcr" {
  account_id   = "gitlab-gcr"
  display_name = "Gitlab-Google Cloud Registry"
}

# Create service account keypair
resource "google_service_account_key" "gitlab-gcr" {
  service_account_id = "${google_service_account.gitlab-gcr.name}"
}

resource "google_project_iam_binding" "gitlab-project" {
  project = "${var.gcp_project_id}"
  role    = "roles/storage.admin"

  members = [
    "${format("serviceAccount:%s", google_service_account.gitlab-gcr.email)}",
  ]
}

# Create a service account for GCR
resource "kubernetes_service_account" "gitlab_gcr" {
  metadata {
    name      = "gitlab-gcr"
    namespace = "${var.project_namespace}"

    labels = "${merge(var.labels,
			map("application-name", "AutoDevOps"),
	  	map("component", "gitlab-kubernetes_service_account"),
	  	map("name", "gitlab-gcr"),
	  	map("instance", "${var.gcp_zone}"),
	  	map("part-of", "gitlab"),
	  	map("app-role", "account")
  )}"
  }

  # Fix bug https://github.com/terraform-providers/terraform-provider-helm/issues/148
  automount_service_account_token = true
}

#resource "kubernetes_cluster_role_binding" "gitlab_gcr" {
  #metadata {
    #name = "gitlab-gcr"
#
    #labels = "${merge(var.labels,
			#map("application-name", "GitLab"),
		  #map("component", "gitlab-kubernetes_cluster_role_binding"),
		  #map("name", "gitlab-gcr"),
		  #map("instance", "${var.gcp_zone}"),
	  	#map("part-of", "gitlab"),
		  #map("app-role", "rbac")
    #)}"
  #}

  #role_ref {
    #kind      = "ClusterRole"
    #name      = "cluster-admin"
    #api_group = "rbac.authorization.k8s.io"
  #}

  #subject {
    #api_group = ""
    #kind      = "ServiceAccount"
    #name      = "${kubernetes_service_account.gitlab_admin.metadata.0.name}"
    #namespace = "${kubernetes_service_account.gitlab_admin.metadata.0.namespace}"
  #}
#}

#data "kubernetes_secret" "gitlab_gcr" {
  #metadata {
    #name      = "${kubernetes_service_account.gitlab_gcr.default_secret_name}"
    #namespace = "${kubernetes_service_account.gitlab_gcr.metadata.0.namespace}"
  #}
#}

# This gets container credentials every time you create a new google_container_cluster
resource "null_resource" "credentials" {
  # The following three lines makes it get the credentials everytime you run apply  # triggers {  # build_number = "${timestamp()}"  # }

  provisioner "local-exec" {
    command = <<EOF
gcloud container clusters get-credentials "${google_container_cluster.gitlab_gke_cluster.name}" --zone="${var.gcp_zone}" --project="${var.gcp_project_id}"
EOF
  }

	depends_on = ["google_container_cluster.gitlab_gke_cluster", "google_container_node_pool.gitlab_premptible_nodes"]
}
