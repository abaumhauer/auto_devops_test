variable "gcp_project_id" {
  description = "Set this to the Google Project ID"
  default     = "premium-highway-238202"
}

variable "gcp_region" {
  description = "Set this to the Google region to run the cluster"
  default     = "us-central1"
}

variable "gcp_zone" {
  description = "Set this to the Google zone to run the cluster"
  default     = "us-central1-f"
}

variable "environment" {
  description = "Set this to the environment tag"
  default     = "development"
}

variable "project_namespace" {
  description = "Namespace to install apps on cluster via GitLab"
  default     = "autodevops"
}

variable "cluster_name" {
  description = "Name of cluster"
  default     = "shitlab"
}

variable "gke_version" {
	description = "Version of Kubernetes to run"
  default = "1.12.7-gke.7"
}

variable "labels" {
  description = "List of key/value pairs to label resouces with"

  default = {
    "owner"    = "abaumhauer"
    "heritage" = "terraform"
  }
}
