# GitLab Auto Devops Demo
## Overview
GitLab Auto Dev Ops is a bit of a black box right now. This demo attempts to expand on the out of the box example. It integrates [Google GKE](https://console.cloud.google.com), [Terraform](https://terraform.io), [Helm](https://helm.io) to make a more realistic demo. Along the way, we'll look at some of the inner workings of how the pipeline works, and link all the scattered documentation to this page.

## Prerequisites
- [ ] You need a working account on Google Cloud. Sign up for $300 worth of free credit [here](https://cloud.google.com/free/).
- [ ] You need a [GitLab](https://www.gitlab.com) account.
- [ ] You need a Linux shell. [Google Cloud Shell](https://console.cloud.google.com/cloudshell/editor?shellonly=true) works just fine.
- [ ] Enable the [Kubernetes Engine API](https://console.cloud.google.com/kubernetes/) after logging into Google Cloud.

### Installing `gcloud` Software Development Kit
Download and install the Google Cloud SDK into your shell:

```shell
curl https://sdk.cloud.google.com | bash
```

After install completes, you are asked to add the SDK to your PATH in `.bashrc`. You need to restart your shell for changes to take effect.

### Authentication and Configuration
Login with your credentials to GCP using account name, password and two-factor authentication code. _(You are using TFA, right? If not, go back and enable it before moving on.)_

```shell
gcloud auth login
```

Set the desired project to bill this activity to, and set the desired zone for running the k8s cluster (saves typing). Use the Cloud Web UI to determine the ID for the project, and choose a zone like `us-central1-f`. Update the values shown below in _<>_.

```shell
gcloud config set project <project_id>
gcloud config set compute/zone <zone>
```

### Installing `kubectl`
Install the command line tool for interacting with Kubernetes.
```shell
gcloud components install kubectl
```

Add the following line to the end of your `.bashrc` to enable command completion for `kubectl`:
```shell
source <(kubectl completion bash)
```
Run that manually to avoid having to log out of the shell again.

### Installing `terraform`
Install the [`terraform`](https://www.terraform.io/downloads.html) command and add it to your path. Terraform allows you to build GCP infrastructure as code. We will use terraform to build a kubernetes cluster for hosting GitLab CI/CD deployed projects (this demo). Most likely you will want the Linux 64 bit version.

```shell
curl
