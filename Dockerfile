FROM infrastructureascode/hello-world:latest AS base
# We want to add a shell to a container that didn't have one
FROM busybox:latest
LABEL maintainer "Andy Baumhauer <andyb@finext.net>"
COPY --from=base /hello_world .
ARG FROM_CICD_ARG="FROM_CICD_ARG_VIA_DOCKERFILE"
ENV FROM_DOCKER_ENV=$FROM_CICD_ARG
ENV GIN_MODE release
EXPOSE 8080

# This starts a web server that says "Hello world" at /
# Returns health status at /health
# Prometheus metrics at /metrics
ENTRYPOINT ["/hello_world"]
