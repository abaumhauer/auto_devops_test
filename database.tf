
# Create a Google Cloud SQL Postgres server for user information
# Use the private IP provisioned in the network module to access
# This failed with error code 409: unknown error on create until I created a db from the web console,
# then it works fine. Clearly the TF and console APIs are different and something else that TF doesn't
# know how to do is being done (creating the private network?)
resource "google_sql_database_instance" "gitlab" {
  name             = "gitlab-prod"   # This name can't be reused after a destroy for a period of time
  database_version = "POSTGRES_9_6"
  region           = "${var.region}"

  settings {
    availability_type = "ZONAL"

    tier = "db-g1-small" # This is the smallest shared CPU database we can use

    location_preference {
      zone = "${var.zone}"
    }

    backup_configuration {
      enabled    = true
      start_time = "03:00"
    }

    maintenance_window {
      day  = "6"
      hour = "7"
    }

    ip_configuration {
      ipv4_enabled    = "false"
      private_network = "${format("projects/%s/global/networks/default", var.project)}"
    }

    user_labels = "${merge(var.labels,
			map("component", "iac_gcp-compute-gitlab-gitlab_sql_database"),
  		map("name", "gitlab-db"),
    	map("version", "1"),
			map("instance", "${var.zone}"),
			map("app-role", "gke")
		)}"
  }
}

# Create a random 64 character password for db user
resource "random_string" "db_password" {
  length  = 64
  special = false # Some special characters don't work yet

  # https://github.com/google-terraform-modules/terraform-google-kubernetes-engine/issues/18
  number = true
  lower  = true
  upper  = true
}

## Create a user for the database
resource "google_sql_user" "gitlab" {
  name     = "gitlab"
  instance = "${google_sql_database_instance.gitlab.name}"
  password = "${random_string.db_password.result}"
}

## Create the database
resource "google_sql_database" "gitlab" {
  name     = "gitlab-production"
  instance = "${google_sql_database_instance.gitlab.name}"
}

# Create a secret so GitLab can access the PG database
resource "kubernetes_secret" "gitlab_db" {
  metadata {
    name      = "gitlab-postgresql-password"
    namespace = "${kubernetes_namespace.gitlab.id}"

    labels = "${merge(var.labels,
			map("application-name", "GitLab"),
			map("component", "iac_gcp-compute-gitlab-kubernetes_secret"),
	  	map("name", "gitlab_db"),
			map("instance", "${var.zone}"),
			map("part-of", "gitlab"),
			map("app-role", "CloudSQL")
  	)}"
  }

  data {
    username = "gitlab"
    password = "${random_string.db_password.result}"
  }

  type = "kubernetes.io/generic"
}
